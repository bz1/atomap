Atomap is a Python library for analysing atomic resolution
scanning transmission electron microscopy images.
It relies on fitting 2-D Gaussian functions to every atomic
column in an image, and automatically finding all the atomic
planes with the largest spacings.

More information can be found on [http://atomap.org](http://atomap.org).

Webpage for the development version: https://gitlab.com/atomap/atomap/builds/artifacts/master/file/pages_development/index.html?job=pages_development_branch
